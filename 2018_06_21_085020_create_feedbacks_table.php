<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Migration auto-generated by Sequel Pro Laravel Export (1.4.1)
 * @see https://github.com/cviebrock/sequel-pro-laravel-export
 */
class CreateFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedbacks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('customer_id');
            $table->tinyInteger('is_satisfy')->default(1);
            $table->string('comment', 255)->nullable();
            $table->text('notes')->nullable();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->nullableTimestamps();
            $table->softDeletes();
            $table->unsignedInteger('merchant_id');
            $table->unsignedInteger('old_id');

            $table->index('merchant_id', 'feedbacks_merchant_id_fk_idx');

            $table->foreign('customer_id', 'feedbacks_customer_id_foreign')->references('id')->on('customers')->onDelete('RESTRICT
')->onUpdate('RESTRICT');
            $table->foreign('merchant_id', 'feedbacks_merchant_id_fk')->references('id')->on('merchants')->onDelete('NO ACTION
')->onUpdate('NO ACTION');

        });

        

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedbacks');
    }
}
