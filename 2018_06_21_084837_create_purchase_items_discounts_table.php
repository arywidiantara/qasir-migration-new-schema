<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Migration auto-generated by Sequel Pro Laravel Export (1.4.1)
 * @see https://github.com/cviebrock/sequel-pro-laravel-export
 */
class CreatePurchaseItemsDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_items_discounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('purchase_item_id');
            $table->tinyInteger('type')->default(1);
            $table->string('name', 255);
            $table->text('description')->nullable();
            $table->double('value')->default(0);
            $table->nullableTimestamps();
            $table->unsignedInteger('merchant_id');
            $table->unsignedInteger('old_id');

            $table->index('merchant_id', 'purchase_items_discounts_idx');

            $table->foreign('merchant_id', 'purchase_items_discounts')->references('id')->on('merchants')->onDelete('NO ACTION
')->onUpdate('NO ACTION');

        });

        

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_items_discounts');
    }
}
